package com.gitek.superlinea;

/**
 * Created by gitek on 23/05/13.
 */
public class Pedido {
    private String id;
    private String kolorea;
    private String material_foto;
    private boolean enproceso;

    public Pedido() {

    }
    public Pedido(String id, String kolorea) {
        this.id = id;
        this.kolorea = kolorea;
        this.setEnproceso(true);
    }


    public Pedido(String id, String kolorea, String material_foto) {
        this.id = id;
        this.kolorea = kolorea;
        this.material_foto = material_foto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKolorea() {
        return kolorea;
    }

    public void setKolorea(String kolorea) {
        this.kolorea = kolorea;
    }

    public boolean isEnproceso() {
        return enproceso;
    }

    public void setEnproceso(boolean enproceso) {
        this.enproceso = enproceso;
    }


    public String getMaterial_foto() {
        return material_foto;
    }

    public void setMaterial_foto(String aterial_foto) {
        material_foto = aterial_foto;
    }
}
