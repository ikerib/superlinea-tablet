package com.gitek.superlinea;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.*;
import android.R;
import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Hacerpost extends AsyncTask<String, Void, Integer> {
        private ProgressDialog progressDialog;
        private SuperlineaActivity activity;
        JSONObject finalResult;

        public Hacerpost(SuperlineaActivity activity, ProgressDialog progressDialog)
        {
            this.activity = activity;
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute()
        {
            progressDialog.show();
        }

        @Override
        protected Integer doInBackground(String... arg0)
        {
            String result = "";
            int responseCode = 0;

            HttpClient client = new DefaultHttpClient();

            final HttpParams par = client.getParams();
            HttpConnectionParams.setConnectionTimeout(par, 100000);
            HttpConnectionParams.setSoTimeout(par, 100000);
            ConnManagerParams.setTimeout(par, 100000);

            try {

                HttpPost httppost = new HttpPost(arg0[0]);

                Log.e("log_iker","Helbidea: " + arg0[0]);

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("materialid", arg0[1]));
                nameValuePairs.add(new BasicNameValuePair("puestoid", arg0[2]));
                nameValuePairs.add(new BasicNameValuePair("lineaid", arg0[3]));
                nameValuePairs.add(new BasicNameValuePair("usuarioid", arg0[4]));
                nameValuePairs.add(new BasicNameValuePair("productoid", arg0[5]));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                Log.e("log_iker", "Datos: " + nameValuePairs.toString());

                int executeCount = 0;
                HttpResponse response;
                do
                {
                    progressDialog.setMessage("Realizando pedido... ("+(executeCount+1)+"/5)");
                    // Execute HTTP Post Request
                    executeCount++;
                    response = client.execute(httppost);
                    responseCode = response.getStatusLine().getStatusCode();
                    // If you want to see the response code, you can Log it
                    // out here by calling:
                    Log.e("log_iker", "statusCode: " + responseCode);
                } while (executeCount < 5 && responseCode == 408);

                BufferedReader rd = new BufferedReader(new InputStreamReader(
                        response.getEntity().getContent()));

                String line;
                while ((line = rd.readLine()) != null)
                {
                    result = line.trim();
                }
                Log.e("log_iker","HABER! => " + result);
                finalResult = new JSONObject(result);
            }
            catch (Exception e) {
                Log.e("log_iker","HOOR2 => " + e.toString());
                responseCode = 408;
                e.printStackTrace();
            }
            return responseCode;
        }

        @Override
        protected void onPostExecute(Integer headerCode)
        {
            progressDialog.dismiss();
            if(headerCode == 202) {
                activity.erantzuna(null);
            } else {
                activity.erantzuna(finalResult);
            }
        }

}
