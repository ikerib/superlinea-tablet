package com.gitek.superlinea;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;
import android.widget.ViewFlipper;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class SuperlineaActivity extends Activity {
    String myPrefUrl = null;
    String myPrefTablet = null;
    String myPrefVideo = null;
    String myPrefImagen = null;
    String myPrefLocalMedia = null;
    String myPrefApiPeticionMaterial = null;
    String myPrefApiCheckPedido = null;
    String myPuestoid = null;
    Map<String, String> losPedidos = new HashMap<String, String>();
    Pedido miPedido = null;
    boolean miPedidoCreado = false;
    ProgressDialog mProgressDialog;
    JSONArray Jarray;
    Bitmap bm;
    List<ImageButton> nirebotoiak = new ArrayList<ImageButton>();
    int numeroBotones = 0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flipper);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        LeerPreferencias();

        Combrobaciones();

        try {
            new task().execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }

    //@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public void erantzuna(JSONObject myJson) {

        if (miPedidoCreado == true) {
            AlertDialog alerta = new AlertDialog.Builder(SuperlineaActivity.this).create();
            alerta.setTitle("PEDIDO EN PROCESO");

            alerta.setMessage("El pedido se ha procesado. Espera.");
            alerta.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alerta.show();
            TextView textView = (TextView) alerta.findViewById(android.R.id.message);
            textView.setTextSize(40);
            String kolorea = null;
            String pedidoid = null;
            String materialfoto = null;
            try {
                kolorea = myJson.getString("kolorea");
                pedidoid = myJson.getString("pedidoid");
                materialfoto = myJson.getString("materialfoto");
                losPedidos.put(materialfoto, pedidoid);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            LeerPreferencias();

            // limpiamos el table layout
            TableLayout table = (TableLayout) findViewById(R.id.tableLayout1);
            table.removeAllViews();
            LinearLayout myContent = (LinearLayout) findViewById(R.id.myContent);
            myContent.removeAllViews();
            new task().execute();

        } else {
            Log.e("log_iker", "PUUUUUT");
            String erantzuna = null;
            try {
                if (myJson != null) {
                    erantzuna = myJson.getString("respuesta");
                } else {
                    erantzuna = "kk";
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("log_iker", "Erantzuna da: " + erantzuna);
            if (erantzuna.trim().equals("OK")) {
                Log.e("log_iker", "Hemen sartu naiz");
                miPedido = null;

                LeerPreferencias();

                // limpiamos el table layout
                TableLayout table = (TableLayout) findViewById(R.id.tableLayout1);
                table.removeAllViews();
                LinearLayout myContent = (LinearLayout) findViewById(R.id.myContent);
                myContent.removeAllViews();
                new task().execute();
            } else {
                Log.e("log_iker", "Beste honetan sartu naiz");
                AlertDialog alerta = new AlertDialog.Builder(SuperlineaActivity.this).create();
                alerta.setTitle("ADVERTENCIA!");

                alerta.setMessage("Hay que leer el Codigo de Barras primero.");
                alerta.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                alerta.show();
                TextView textView = (TextView) alerta.findViewById(android.R.id.message);
                textView.setTextSize(40);
            }


        }
    }

    public void LeerPreferencias() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        myPrefUrl = sp.getString("myPrefUrl", "http://192.168.1.16/api/instrucciones/");
        myPrefTablet = sp.getString("myPrefTablet", "tablet3");
        myPrefVideo = sp.getString("myPrefVideo", "http://192.168.1.16/video/");
        myPrefImagen = sp.getString("myPrefImagen", "http://192.168.1.16/imagen/");
        myPrefLocalMedia = sp.getString("myPrefLocalMedia", "/SUPERLINEA/media/");
        myPrefApiPeticionMaterial = sp.getString("myPrefApiPeticionMaterial", "http://192.168.1.16/api/pedidos");
        myPrefApiCheckPedido = sp.getString("myPrefApiCheckPedido", "http://192.168.1.16/pedidopendiente/");
    }

    private void Combrobaciones() {
        // VIDEO
        File folder = new File(Environment.getExternalStorageDirectory() + myPrefLocalMedia);

        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdirs();
        }
        if (success) {
            Log.e("log_iker", "Creado: " + myPrefLocalMedia);
        } else {
            Log.e("log_iker", "ERROR DE CREACION: " + myPrefLocalMedia);
            Toast toast1 =
                    Toast.makeText(getApplicationContext(),
                            "Ha habido un problema. Revise los permisos.", Toast.LENGTH_SHORT);

            toast1.show();
        }
    }

    public void BurutuDeskargak() throws JSONException {

        // instantiate it within the onCreate method
        mProgressDialog = new ProgressDialog(SuperlineaActivity.this);
        mProgressDialog.setMessage("Buscando instrucciones...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(100);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

        // *********************************************************************
        // *** Gauzak jetsi
        // *********************************************************************

        JSONObject Jasonobject = null;
        Jasonobject = Jarray.getJSONObject(0);

        JSONArray Jpuestos = Jasonobject.getJSONArray("puestos");
        JSONObject jp = null;
        jp = Jpuestos.getJSONObject(0);
        final String PUESTOID = jp.getString("id");
        myPuestoid = PUESTOID;


        String milineaid = null;
        try {
            JSONObject jsonLinea = jp.getJSONObject("linea");
            milineaid = jsonLinea.getString("id");

        } catch (Exception e) {
            Log.e("log_iker", "no existe linea => " + e.toString());
        }
        final String LINEAID = milineaid;

        String miusuario = null;
        try {
            JSONObject jsonUsuario = jp.getJSONObject("usuario");
            miusuario = jsonUsuario.getString("id");
        } catch (Exception e) {
            Log.e("log_iker", "No existe usuario " + e.toString());
        }
        final String USUARIOID = miusuario;

        String miproducto = null;
        String miprodname = null;
        try {
            JSONObject jsonProducto = jp.getJSONObject("producto");
            miproducto = jsonProducto.getString("id");
            miprodname = jsonProducto.getString("nombre");
        } catch (Exception e) {
            Log.e("log_iker", "No existe producto " + e.toString());
        }
        final String PRODUCTOID = miproducto;
        final String PRODUCTNAME = miprodname;

        // ********************************************
        // ** Titulo Instrucción
        // ********************************************

        String nombre = Jasonobject.getString("nombre");
        TextView txtTitulo = (TextView) findViewById(R.id.myTitle);
        if (txtTitulo!=null) {
//            txtTitulo.setText(PRODUCTNAME + ":\n" + nombre);
              txtTitulo.setText(nombre);
        }

        boolean seMuestraVideo = false;

        // ********************************************
        // ** Video
        // ********************************************
        try {
            Log.e("log_iker", "Hmen");
            String video = Jasonobject.getString("video");
            File file = new File(Environment.getExternalStorageDirectory() + myPrefLocalMedia, video);
            if (!file.exists()) {
                Log.e("log_iker", "No existe");
                DownloadFile downloadFile = new DownloadFile();
//                downloadFile.execute(myPrefVideo + video);
                downloadFile.execute(myPrefVideo + video).get();
            }

            LinearLayout myContent = (LinearLayout) findViewById(R.id.myContent);

            VideoView videoView = new VideoView(this);
            MediaController mediaController = new MediaController(SuperlineaActivity.this);
            videoView.setMediaController(mediaController);
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mp.setLooping(true);
                }
            });
            videoView.setVideoPath(Environment.getExternalStorageDirectory() + myPrefLocalMedia + video);

            myContent.addView(videoView);
            videoView.start();


            seMuestraVideo = false;

        } catch (Exception e) {
            Log.e("log_iker", "Ez dago bido inforik");
        }


        if (seMuestraVideo == false) {
            // ********************************************
            // ** Argazkia
            // ********************************************
            try {
                String argazkia = Jasonobject.getString("foto");
                File file2 = new File(Environment.getExternalStorageDirectory() + myPrefLocalMedia, argazkia);
                if (!file2.exists()) {
                    Log.e("log_iker", "Argazkia ez da existitzen. Deskargatzen.");
                    DownloadFile downloadFile = new DownloadFile();
                    downloadFile.execute(myPrefImagen + argazkia).get();
                }

                LinearLayout myContent = (LinearLayout) findViewById(R.id.myContentd16);

                ImageView img = new ImageView(this);
                Bitmap bmp = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + myPrefLocalMedia + argazkia);
                Log.e("log_iker", "Kargatu argazkia hemendik: " + Environment.getExternalStorageDirectory() + myPrefLocalMedia + argazkia);
                img.setImageBitmap(bmp);

                myContent.addView(img);


            } catch (Exception e) {
                Log.e("log_iker", "Ez dago argazki inforik");
            }
        }


        // ********************************************
        // ** Materiales
        // ********************************************
        JSONArray Jmateriales = Jasonobject.getJSONArray("materiales");
        for (int j = 0; j < Jmateriales.length(); j++) {
            JSONObject jo = null;
            jo = Jmateriales.getJSONObject(j);
            final String mat_id = jo.getString("id");
            String mat_nombre = jo.getString("nombre");
            final String mat_foto = jo.getString("foto");

            Log.e("log_iker", "Material: " + mat_id + " // " + mat_nombre + " // " + mat_foto);
            try {
                DownloadFile downloadFile = new DownloadFile();
                downloadFile.execute(myPrefImagen + mat_foto).get();
            } catch (Exception e) {
                Log.e("log_iker", "Ez dago argazki inforik");
            }


            TableLayout table = (TableLayout) findViewById(R.id.tableLayout1);

            int buttonsInRow = 0;
            int numRows = table.getChildCount();
            TableRow row = null;
            if (numRows > 0) {
                row = (TableRow) table.getChildAt(numRows - 1);
                buttonsInRow = row.getChildCount();
            }

            if (numRows == 0 || buttonsInRow == 3) {
                row = new TableRow(this);
                table.addView(row);
                buttonsInRow = 0;
            }
            if (buttonsInRow < 3) {
                Bitmap bmp = BitmapFactory.decodeFile(Environment.getExternalStorageDirectory() + myPrefLocalMedia + mat_foto);
                Log.e("log_iker", "");
                Log.e("log_iker", "Material argazkia hemendik: " + Environment.getExternalStorageDirectory() + myPrefLocalMedia + mat_foto);
                Log.e("log_iker", "");
                final ImageButton b = new ImageButton(this);
                b.setScaleType(ImageView.ScaleType.FIT_CENTER);

                b.setPadding(20, -10, 20, 20);
                b.setAdjustViewBounds(true);
                b.setImageBitmap(bmp);
                b.setTag(mat_foto);

                b.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Log.e("log_iker", "Klok klok klok ");
                        b.setId(888);
                        nirebotoiak.add(b);
                        ProgressDialog progressDialog = new ProgressDialog(SuperlineaActivity.this);
                        progressDialog.setMessage("Realizando el pedido...");
                        progressDialog.setCancelable(false);
                        //if ( (miPedido == null) ||  ( !b.getTag().equals(losPedidos.put(mat_foto) ))) {
                        if (losPedidos.containsKey(b.getTag()) == false) {
                            Log.e("log_iker", "Hago post! Creo pedido!!");
                            miPedidoCreado = true;
                            Hacerpost loginTask = new Hacerpost(SuperlineaActivity.this, progressDialog);
                            loginTask.execute(myPrefApiPeticionMaterial, mat_id, PUESTOID, LINEAID, USUARIOID, PRODUCTOID);
                        } else {
                            Log.e("log_iker", "Hago put! Actualizo el pedido pedido!!");
                            miPedidoCreado = false;
                            Hacerput puttask = new Hacerput(SuperlineaActivity.this, progressDialog);

                            puttask.execute(myPrefApiPeticionMaterial, losPedidos.get(b.getTag()));
                        }
                    }
                });
                numeroBotones += 1;
                row.addView(b, 250, 250);
            }
            mProgressDialog.dismiss();
        }

    }

    private class task extends AsyncTask<String, String, Void> {
        private ProgressDialog progressDialog = new ProgressDialog(SuperlineaActivity.this);
        InputStream is = null;
        String result = "";

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Descargando datos...");
            progressDialog.show();
            progressDialog.setOnCancelListener(new OnCancelListener() {

                @Override
                public void onCancel(DialogInterface arg0) {
                    task.this.cancel(true);
                }
            });
        }

        @Override
        protected Void doInBackground(String... params) {
            String url_select = myPrefUrl + myPrefTablet;
            Log.e("log_iker", "helbidea => " + url_select);
            HttpClient httpClient = new DefaultHttpClient();

            try {
                HttpGet httpGet = new HttpGet(url_select);
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            } catch (Exception e) {
                Log.e("log_tag", "(Deskargak) Error in http connection " + e.toString());
                Log.e("log_tag", "(Deskargak) Zer egingo zaio ba!");
                result = null;
                return null;
            }
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
            } catch (Exception e) {
                Log.e("log_tag", "(Deskargak) Error converting result " + e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if (result != null) {
                try {
                    Jarray = new JSONArray(result);
                    for (int i = 0; i < Jarray.length(); i++) {
                        JSONObject Jasonobject = null;
                        Jasonobject = Jarray.getJSONObject(i);
                        String nombre = Jasonobject.getString("nombre");
                        TextView txtTitulo = (TextView) findViewById(R.id.myTitle);
                        if (txtTitulo!=null) {
                            txtTitulo.setText(nombre);
                        }
                    }
                    this.progressDialog.dismiss();

                    BurutuDeskargak();

                    new checkpedidopendiente().execute();


                } catch (Exception e) {
                    Log.e("log_tag", "Error parsing data " + e.toString());
                    Log.e("log_tag", "Jasotako datuak okerrak dira1.");
                    this.progressDialog.dismiss();
                    AlertDialog alerta = new AlertDialog.Builder(SuperlineaActivity.this).create();
                    alerta.setTitle("Errorea");
                    alerta.setMessage("NO HAY PRODUCTO LANZADO. \n HABLA CON TU ENCARGADO.");
                    alerta.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                        }
                    });

                    alerta.show();
                }
            } else {
                Log.e("log_tag", "Ez da ezer aurkitu, agian ez da URL egokia.");
                this.progressDialog.dismiss();
                AlertDialog alerta = new AlertDialog.Builder(SuperlineaActivity.this).create();
                alerta.setTitle("Error");
                alerta.setMessage("No se han encontrado instrucciones.");
                alerta.setButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
                    }
                });
                alerta.show();
            }
        }
    }

    private class DownloadFile extends AsyncTask<String, Integer, String> {
        @Override
        protected String doInBackground(String... sUrl) {
            try {
                URL url = new URL(sUrl[0]);
                String fileName = sUrl[0].substring(sUrl[0].lastIndexOf('/') + 1);
                URLConnection connection = url.openConnection();
                connection.connect();
                int fileLength = connection.getContentLength();

                OutputStream output;
                // download the file
                InputStream input = new BufferedInputStream(url.openStream());

                output = new FileOutputStream(Environment.getExternalStorageDirectory() + myPrefLocalMedia + fileName);

                byte data[] = new byte[1024];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
                Log.e("log_iker", "Jeitsi da! => " + Environment.getExternalStorageDirectory() + myPrefLocalMedia + fileName);
            } catch (Exception e) {
                Log.e("log_iker", "Jeisten horror! => " + e.toString());
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            mProgressDialog.setProgress(progress[0]);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.superlinea, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        ViewFlipper vf = (ViewFlipper) findViewById( R.id.view_flipper );

        switch (item.getItemId()) {
            case R.id.prefs:
                PackageManager manager = this.getPackageManager();
                PackageInfo info = null;
                try {
                    info = manager.getPackageInfo(this.getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }
                Toast.makeText(this,
                        "PackageName = " + info.packageName + "\nVersionCode = "
                                + info.versionCode + "\nVersionName = "
                                + info.versionName + "\nPermissions = " + info.permissions, Toast.LENGTH_SHORT).show();

                startActivity(new Intent(SuperlineaActivity.this,
                        Prefs.class));
                break;

            case R.id.actualizar:
                LeerPreferencias();

                // limpiamos el table layout
                TableLayout table = (TableLayout) findViewById(R.id.tableLayout1);
                table.removeAllViews();
                LinearLayout myContent = (LinearLayout) findViewById(R.id.myContent);
                myContent.removeAllViews();
                new task().execute();

                break;

            case R.id.action_video:
                Toast.makeText(this,"Cargando Video instrucción", Toast.LENGTH_SHORT).show();
                AnimationFactory.flipTransition(vf, AnimationFactory.FlipDirection.LEFT_RIGHT);

                if (item.getIcon().getConstantState().equals(
                        getResources().getDrawable(R.drawable.ic_action_picture).getConstantState()
                )) {
                    item.setIcon(R.drawable.ic_action_video);
                } else {
                    item.setIcon(R.drawable.ic_action_picture);
                }

        }
        return false;
    }

    private class checkpedidopendiente extends AsyncTask<String, String, Void> {
        private ProgressDialog progressDialog = new ProgressDialog(SuperlineaActivity.this);
        InputStream is = null;
        String result = "";

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Comprobar si existen pedidos pendientes...");
            progressDialog.show();
            progressDialog.setOnCancelListener(new OnCancelListener() {

                @Override
                public void onCancel(DialogInterface arg0) {
                    checkpedidopendiente.this.cancel(true);
                }
            });
        }

        @Override
        protected Void doInBackground(String... params) {
            String url_select = myPrefApiCheckPedido + myPuestoid;
            Log.e("log_iker", "helbidea pedidopendiente => " + url_select);
            HttpClient httpClient = new DefaultHttpClient();

            try {
                HttpGet httpGet = new HttpGet(url_select);
                HttpResponse httpResponse = httpClient.execute(httpGet);
                HttpEntity httpEntity = httpResponse.getEntity();

                //read content
                is = httpEntity.getContent();
            } catch (Exception e) {
                Log.e("log_tag", "(Deskargak) Error in http connection " + e.toString());
                Log.e("log_tag", "(Deskargak) Zer egingo zaio ba!");
                result = null;
                return null;
            }
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String line = "";
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
            } catch (Exception e) {
                Log.e("log_tag", "(Deskargak) Error converting result " + e.toString());
            }
            return null;

        }

        @Override
        protected void onPostExecute(Void v) {
            if (result != null) {
                try {
                    losPedidos.clear();
                    JSONArray Jarray1 = new JSONArray(result);
                    //for(int i=0;i<=Jarray.length();i++) {
                    for (int i = 0; i <= numeroBotones; i++) {

                        JSONObject Jasonobject = null;

                        Jasonobject = Jarray1.getJSONObject(i);

                        String pedidoid = Jasonobject.getString("id");
                        String kolorea = Jasonobject.getString("kolorea");


                        //material
                        JSONObject jo = new JSONObject(Jasonobject.getString("material"));

                        final String mat_id = jo.getString("id");
                        String mat_nombre = jo.getString("nombre");
                        final String mat_foto = jo.getString("foto");

                        losPedidos.put(mat_foto, pedidoid);
                        miPedido = new Pedido(pedidoid, kolorea, mat_foto);
                        Log.e("log_iker", "pedido creado");

                        TableLayout parent = (TableLayout) findViewById(R.id.tableLayout1);

                        View child = parent.getChildAt(0);

                        Log.e("log_iker", child.toString());
                        if (child instanceof TableRow) {
                            TableRow row = (TableRow) child;
                            for (int j = 0; j < row.getChildCount(); j++) {

                                ImageButton ib2 = (ImageButton) row.getChildAt(j);

                                if (ib2.getTag().equals(mat_foto)) {
                                    Log.e("log_iker", "Pediduan botoia topau daaaaiiiiiii");
                                    Log.e("log_iker", " Foto da: => " + ib2.getTag());

                                    if (miPedido.getKolorea().equals("kuadro-gorria")) {
                                        ib2.setBackgroundColor(getResources().getColor(R.color.gorria));
                                    } else if (miPedido.getKolorea().equals("kuadro-berdea")) {
                                        ib2.setBackgroundColor(getResources().getColor(R.color.berdea));
                                    } else if (miPedido.getKolorea().equals("kuadro-urdin")) {
                                        ib2.setBackgroundColor(getResources().getColor(R.color.urdina));
                                    } else {
                                        ib2.setBackgroundColor(getResources().getColor(R.color.beltza));
                                    }
                                }
                            }
                        }
                    }


                    this.progressDialog.dismiss();

                } catch (Exception e) {
                    Log.e("log_tag", "Error parsing data " + e.toString());
                    Log.e("log_tag", "NO HAY PEDIDO.");
                    miPedido = null;
                    this.progressDialog.dismiss();
//                    AlertDialog alerta   = new AlertDialog.Builder(SuperlineaActivity.this).create();
//                    alerta.setTitle("Errorea (CheckPedido)");
//                    alerta.setMessage("Jasotako datuak okerrak dira2: " + e.toString());
//                    alerta.setButton("OK", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int which) {
//                            Toast.makeText(getApplicationContext(), "You clicked on OK", Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                    alerta.show();
                }
            }
        }
    }

}
