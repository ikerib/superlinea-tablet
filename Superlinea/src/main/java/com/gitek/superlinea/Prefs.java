package com.gitek.superlinea;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;

public class Prefs extends PreferenceActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.prefs);
        try {
            SharedPreferences sp = getPreferenceScreen().getSharedPreferences();

            // URL
            EditTextPreference editTextPref1 = (EditTextPreference) findPreference("myPrefUrl");
            editTextPref1.setSummary(sp.getString("myPrefUrl", "http://DOMINIO/api/instrucciones/"));

            // TABLET
            EditTextPreference editTextPref2 = (EditTextPreference) findPreference("myPrefTablet");
            editTextPref2.setSummary(sp.getString("myPrefTablet", "SuperlineaXXX"));

            // Video URL
            EditTextPreference editTextPref3 = (EditTextPreference) findPreference("myPrefVideo");
            editTextPref3.setSummary(sp.getString("myPrefVideo", "http://DOMINIO/video/"));

            // Imagen URL
            EditTextPreference editTextPref4 = (EditTextPreference) findPreference("myPrefImagen");
            editTextPref4.setSummary(sp.getString("myPrefImagen", "http://DOMINIO/images/uploads/"));

            // LocalMedia
            EditTextPreference editTextPref5 = (EditTextPreference) findPreference("myPrefLocalMedia");
            editTextPref5.setSummary(sp.getString("myPrefLocalMedia", "/SUPERLINEA/media/"));

            // ApiPeticionMaterial
            EditTextPreference editTextPref6 = (EditTextPreference) findPreference("myPrefApiPeticionMaterial");
            editTextPref6.setSummary(sp.getString("myPrefApiPeticionMaterial", "http://DOMINIO/api/pedidos"));

            // ApiCheckPedido
            EditTextPreference editTextPref7 = (EditTextPreference) findPreference("myPrefApiCheckPedido");
            editTextPref7.setSummary(sp.getString("myPrefApiCheckPedido", "http://DOMINIO/api/pedidopendiente/"));

        } catch (Exception e) {
            Log.e("log_iker", "Aixxxx => " + e.toString());
        }

        Preference button = (Preference)getPreferenceManager().findPreference("cmdCargar");
        if (button != null) {
            button.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference arg0) {

                    SharedPreferences sp = getPreferenceScreen().getSharedPreferences();
                    SharedPreferences sp1 = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                    String opt = sp1.getString("myOpt", "debug");

                    EditTextPreference editTextPref1 = (EditTextPreference) findPreference("myPrefUrl");
                    EditTextPreference editTextPref2 = (EditTextPreference) findPreference("myPrefTablet");
                    EditTextPreference editTextPref3 = (EditTextPreference) findPreference("myPrefVideo");
                    EditTextPreference editTextPref4 = (EditTextPreference) findPreference("myPrefImagen");
                    EditTextPreference editTextPref5 = (EditTextPreference) findPreference("myPrefLocalMedia");
                    EditTextPreference editTextPref6 = (EditTextPreference) findPreference("myPrefApiPeticionMaterial");
                    EditTextPreference editTextPref7 = (EditTextPreference) findPreference("myPrefApiCheckPedido");

                    if ( opt.equals("debug")) {
                        editTextPref1.setText("http://10.241.70.6/api/instrucciones/");
                        editTextPref2.setText("superlinea1");
                        editTextPref3.setText("http://10.241.70.6/video/");
                        editTextPref4.setText("http://10.241.70.6/images/uploads/");
                        editTextPref5.setText("/SUPERLINEA/media/");
                        editTextPref6.setText("http://10.241.70.6/api/pedidos");
                        editTextPref7.setText("http://10.241.70.6/api/pedidopendiente/");
                    } else if ( opt.equals("prod")) {
                        editTextPref1.setText("http://superlinea.grupogureak.com/api/instrucciones/");
                        editTextPref2.setText("SuperlineaXXX");
                        editTextPref3.setText("http://superlinea.grupogureak.com/video/");
                        editTextPref4.setText("http://superlinea.grupogureak.com/images/uploads/");
                        editTextPref5.setText("/SUPERLINEA/media/");
                        editTextPref6.setText("http://superlinea.grupogureak.com/api/pedidos");
                        editTextPref7.setText("http://superlinea.grupogureak.com/api/pedidopendiente/");
                    }


                    return true;
                }
            });
        }

    }

    protected void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference pref = findPreference(key);
        if (pref instanceof EditTextPreference) {
            EditTextPreference etp = (EditTextPreference) pref;
            pref.setSummary(etp.getText());
        }
    }


}
