package com.gitek.superlinea;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Hacerput extends AsyncTask<String, Void, Integer> {
        private ProgressDialog progressDialog;
        private SuperlineaActivity activity;
        JSONObject finalResult;

        public Hacerput(SuperlineaActivity activity, ProgressDialog progressDialog)
        {
            this.activity = activity;
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute()
        {
            progressDialog.show();
        }

        @Override
        protected Integer doInBackground(String... arg0)
        {
            String result = "";
            int responseCode = 0;

            HttpClient client = new DefaultHttpClient();

            final HttpParams par = client.getParams();
            HttpConnectionParams.setConnectionTimeout(par, 100000);
            HttpConnectionParams.setSoTimeout(par, 100000);
            ConnManagerParams.setTimeout(par, 100000);

            try {

                HttpPut put = new HttpPut(arg0[0] + "/" + arg0[1]);

                Log.e("log_iker","Helbidea: " + arg0[0]+ "/" + arg0[1]);
                Log.e("log_iker","Hona noa: put => " + arg0[0]);

                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("pedidoid", arg0[1]));
                put.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                int executeCount = 0;
                HttpResponse response;
                do
                {
                    progressDialog.setMessage("Logging in.. ("+(executeCount+1)+"/5)");
                    // Execute HTTP Post Request
                    executeCount++;
                    response = client.execute(put);

                    responseCode = response.getStatusLine().getStatusCode();
                    Log.e("log_iker","--");
                    Log.e("log_iker", "Status kodea da: => " + responseCode);
                    Log.e("log_iker","--");
                    // If you want to see the response code, you can Log it
                    // out here by calling:
                    Log.e("log_iker", "statusCode: " + responseCode);
                } while (executeCount < 5 && responseCode == 408);

                if (responseCode != 204) {
                    BufferedReader rd = new BufferedReader(new InputStreamReader(
                            response.getEntity().getContent()));

                    String line;
                    while ((line = rd.readLine()) != null)
                    {
                        result = line.trim();
                    }
                    Log.e("log_iker","HABER! => " + result);
                    finalResult = new JSONObject(result);
                }
            }
            catch (Exception e) {
                Log.e("log_iker","HOOR2 => " + e.toString());
                responseCode = 408;
                e.printStackTrace();
            }
            return responseCode;
        }

        @Override
        protected void onPostExecute(Integer headerCode)
        {
            progressDialog.dismiss();
            if(headerCode == 202) {
                activity.erantzuna(null);
            } else {
                activity.erantzuna(finalResult);
            }
        }

}
